const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',

    entry: {
        index: './src/index.js'
    },

    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'resolve-url-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: [
                                './node_modules/hamburgers/_sass/hamburgers',
                                './node_modules/reset-css/sass'
                            ],
                            sourceMap: true,
                            sourceMapContents: false
                        }
                    }
                ]
            }
        ]
    },

    devServer: {
        port: 8080,
        open: true,
        compress: true
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true,
            chunks: ['index'],
            filename: 'index.html'
        }),

        new HtmlWebpackPlugin({
            template: './src/contacts.html',
            inject: true,
            chunks: ['index'],
            filename: 'contacts.html'
        })
    ]
};
